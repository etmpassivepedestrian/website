﻿
export class Event {
    public nodesAttached: string[];
    constructor(
        public id: string,
        public name: string,
        public location: string,
        public startDate: Date,
        public endDate: Date,
        public organiser: string
        
    )
    {
        
    }
   
}