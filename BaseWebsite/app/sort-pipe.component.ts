﻿import { Pipe, PipeTransform } from "@angular/core";

import {Event} from './event';

@Pipe({
    name: "sort",
    //set to false so it will always update
    pure: false
})
export class SortPipe implements PipeTransform {
    transform(array: Event[], args: any): Event[] {
        
        /* javascript is async, so could be that the pipe is called before
         the list is created, in this case we do nothing
        returning the array as it is */
        if (array === null) return null;
        array.sort((a, b) => {
            if (a.name < b.name) {
                return -1;
                
            } else if (a.name > b.name) {
                return 1;
            } else {
                return 0;
            }
        });
        return array;
    }
}