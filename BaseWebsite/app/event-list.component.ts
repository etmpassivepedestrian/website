﻿import { Component, OnInit} from '@angular/core';
import { Event } from './event';
import { EventService } from './event.service';
import { Router } from '@angular/router';
import {EventDisplayComponent} from './event-display.component';
import {SortPipe} from './sort-pipe.component';

//Build component sections
@Component({
    selector: 'event-list',
    templateUrl: 'app/event-list.component.html',
    styleUrls: ['app/styles/customfonts.css', 'app/styles/event-list.component.css'],

    //This pipe is used in the html as 'sort' 
    pipes: [SortPipe]
})

export class EventListComponent implements OnInit {

    errorMessage: string;
    events: Event[] = [];
    mode = 'Observable';
    selectedEvent: Event;
  
    constructor(private eventService: EventService, private router: Router) { }

    //On creation of the component, run the getEvents function
    ngOnInit()
    {
        this.getEvents();
    }

    //Retrive events from the database using the Event Service
    getEvents() {

        //Call the Event Service 
        //Subscribe to the Observable Object returned and assign the 'events' array to this value
        this.eventService.getEvents()
            .subscribe(
            events => this.events = events);

        
    }
    //Currently unused 
    onSelect(event: Event) {
    this.selectedEvent = event;
   
    }
    
    //Generates an appropriate router link for the selected event
    //Then navigates to this dynamically assigned link
    gotoDetail(event: Event) {
        let link = ['/detail', event.id];
        this.router.navigate(link);
    }

  

}
