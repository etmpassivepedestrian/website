﻿import { Injectable }     from '@angular/core';
import { Http, Response } from '@angular/http';
import { Node }           from './node';
import { Observable }     from 'rxjs/Observable';
import './rxjs-operators';
import { Headers, RequestOptions } from '@angular/http';

//Retrieves and sends data to and from the server
@Injectable()
export class NodeService {
    constructor(private http: Http) { }
    private nodesUrl = 'http://ltnodeserver.no-ip.org/api/nodes';  // URL to web API

    getNodes(): Observable<Node[]> {
        return this.http.get(this.nodesUrl)
            .map(this.extractData)
            .catch(this.handleError);
    }
   
    //Converts response object to readable json format
    private extractData(res: Response) {
        let body = res.json();
        return body.data || {};
    }
    private handleError(error: any) {
        // In a public app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

    //Adds node to database
    addNode(value: string): Observable<Node> {


         //Turn string format into javascript json format 
        //Generate http Headers
        //Generate request options
        let body = JSON.stringify(value);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        //Post request 
        return this.http.post(this.nodesUrl, body, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    getNode(id: string) {
        return this.getNodes()
            .map(nodes => nodes.find(node => node.id == id));
    }

}
