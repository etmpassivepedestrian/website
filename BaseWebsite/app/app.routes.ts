﻿import { provideRouter, RouterConfig }  from '@angular/router';
import {EventFormComponent} from './event-form.component';
import {EventDisplayComponent} from './event-display.component';
import {EventListComponent} from './event-list.component';
import {NodeFormComponent} from './node-form.component';
import {NodeListComponent} from './node-list.component';
import {HomeComponent} from './home.component';
import {HelpComponent} from './help.component';
import {EventEditComponent} from './event-edit.component';
import {EventNodeAttachComponent} from './event-node-attach.component';

//Assign component routes 
//Path: the URL to display
//Component: The Component to route to
const routes: RouterConfig = [
    {
        path: 'detail/:id',
        component: EventDisplayComponent
    
    },
    {
        path: 'edit/:id',
        component: EventEditComponent

    },
    {
        path: 'attach/:id',
        component: EventNodeAttachComponent
    },
    {
        path: 'home',
        component: HomeComponent
    },
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    },
    {
        path: 'NewEventForm', 
        component: EventFormComponent
    },
    {
        path: 'NewNodeForm',
        component: NodeFormComponent
    },
    {
        path: 'EventList',
        component: EventListComponent
    },
    {
        path: 'NodeList',
        component: NodeListComponent
    },
    {
        path: 'Help',
        component: HelpComponent
    }
];

//Export routes 
export const APP_ROUTER_PROVIDERS = [
    provideRouter(routes)
];
