﻿import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import {NodeService} from './node.service';
import {EventService} from './event.service';
import { EventFormComponent } from './event-form.component';
import { NodeFormComponent } from './node-form.component';
import {EventDisplayComponent} from './event-display.component';
import {HelpComponent} from './help.component';
// Add the RxJS Observable operators we need in this app.
import './rxjs-operators';


import { EventListComponent }  from './event-list.component';

@Component({
    selector: 'my-app',
    templateUrl: 'app/app.component.html',
    styleUrls: ['app/styles/app.component.css'],

    directives: [ROUTER_DIRECTIVES, EventFormComponent, EventListComponent, NodeFormComponent],
    providers: [
        NodeService,
        EventService
    ]
})
export class AppComponent {
    title = 'ATLUS';
}

