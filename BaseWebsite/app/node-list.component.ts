﻿import { Component, OnInit} from '@angular/core';
import { Node } from './node';
import { NodeService } from './node.service';
import { Router } from '@angular/router';

//Build component sections
@Component({
    selector: 'node-list',
    templateUrl: 'app/node-list.component.html',
    styleUrls: ['app/styles/customfonts.css', 'app/styles/event-list.component.css'],

    
})

export class NodeListComponent implements OnInit {

    errorMessage: string;
    nodes: Node[] = [];
    mode = 'Observable';
    selectedNode: Node;

    constructor(private nodeService: NodeService, private router: Router) { }

    //On creation of the component, run the getNodes function
    ngOnInit() {
        this.getNodes();
    }

    //Retrive nodes from the database using the Node Service
    getNodes() {

        //Call the Node Service 
        //Subscribe to the Observable Object returned and assign the 'nodes' array to this value
        this.nodeService.getNodes()
            .subscribe(
            nodes => this.nodes = nodes);
        


    }
    //Currently unused 
    onSelect(node: Node) {
        this.selectedNode = node;

    }

    //Generates an appropriate router link for the selected node
    //Then navigates to this dynamically assigned link
    //Unused for sensors, may be used later.
    gotoDetail(node: Node) {
        let link = ['/detail', node.id];
        this.router.navigate(link);
    }



}
