﻿import { Component } from '@angular/core';
import { NgForm }    from '@angular/forms';
import { Node }    from './node';
import { NodeService } from './node.service';
@Component({
    selector: 'node-form',
    templateUrl: 'app/node-form.component.html',
    styleUrls: ['app/styles/customFonts.css', 'app/styles/event-form.component.css']
})
export class NodeFormComponent {

    //Default information to populate form with
    model = new Node('','',false);

    errorMessage: string;
    nodes: Node[] = [];
    submitted = false;
    active = true;
    confirm = false;

    constructor(private nodeService: NodeService) { }

    //Code to execute when the form is submitted
    onSubmit(value: string) {
        this.addNode(value);
        this.submitted = true;
        this.newNode();
        this.confirm = false;
    }

    //Refresh form information
    newNode() {
        this.model = new Node('','',false);
        this.active = false;
        setTimeout(() => this.active = true, 0);
    }

    //Toggles confirm for showing confirmation div tag
    showConfirm() {
        this.confirm = true;
    }

    hideConfirm() {
        this.confirm = false;
    }

    addNode(value: string) {
        if (!value) { return; }
        //Call the Node Service addEvent function
        //Subscribe to the Observable Node Object data stream and push new node to the end of the array 
        //Return an error if problems occur in the service 
        this.nodeService.addNode(value)
            .subscribe(
            node => this.nodes.push(node),
            error => this.errorMessage = <any>error);
    }



}