﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Event } from './event';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { EventService } from './event.service';
import { NgForm }    from '@angular/forms';

//Build component sections
@Component({
    selector: 'my-event-edit',
    templateUrl: 'app/event-edit.component.html',
    styleUrls: ['app/styles/customfonts.css', 'app/styles/event-form.component.css']


})
export class EventEditComponent implements OnInit, OnDestroy {

    // Initialise variables
    event: Event;
    id: string;
    errorMessage: string;

    //Model for data binding between form and component
    model = new Event('', '', '', new Date(), new Date(), '');
    today = new Date();
    
    

    //Booleans
    confirmEventDelete = false;
    submitted = false;
    active = true;
    confirm = false;
   

    constructor(
        private eventService: EventService,
        private router: Router,
        private route: ActivatedRoute) {
    }

    //On component initialisation 
    ngOnInit() {

        //read id information passed in the URL
        this.route.params.subscribe(params => {
            this.id = params['id'];
        });
        //call Event Service function
        //Retrieve and assign the Event object returned by the function and subscribe to the data stream
        this.eventService.getEvent(this.id).subscribe(events => this.event = events);
        
    }


    ngOnDestroy() {
       

    }

    deleteEvent() {
        this.eventService.deleteEvent(this.event.id).subscribe();
        this.hideEventDeleteConfirm();

        //To allow the server enough time to delete the event we need a short timeout before moving back to the event list
        setTimeout(() => {
            let link = ['/EventList'];
            this.router.navigate(link);

        }, 500);
    };

    //Toggles confirm for showing confirmation div tags
    showEventDeleteConfirm() {
        this.confirmEventDelete = true;
        this.confirm = false;
    }

    hideEventDeleteConfirm() {
        this.confirmEventDelete = false;
    }
   

    //Toggles confirm for showing confirmation div tag
    showConfirm() {
        this.confirm = true;
        this.confirmEventDelete = false;
    }

    hideConfirm() {
        this.confirm = false;
    }

    //Code to be executed when the form is submitted 
    //Calls updateEvent to send form data to the database
    onSubmit(value: string) {
        this.updateEvent(this.event.id,value);
        
        this.submitted = true;
        this.confirm = false;

      
        //To allow the server enough time to update the details we need a short timeout before moving back to the event's details
        setTimeout(() =>
        {
            window.history.back();
            
        }, 500);
       
    }

    //Called to send form data to the database
    //Value: Form values in string format
    updateEvent(id:string,value: string) {
        if (!value) { return; }
        //Call the Event Service addEvent function
        //Subscribe to the Observable Event Object 
        //Return an error if problems occur in the service 

        this.eventService.updateEvent(id,value) 
            .subscribe(
            error => this.errorMessage = <any>error);
    }

   
}