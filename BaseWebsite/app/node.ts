﻿export class Node {

    constructor(
        public id: string,
        public serial: string,
        public checked: boolean

    ) { }
}