﻿import { Component, OnInit, OnDestroy, AfterViewChecked } from '@angular/core';
import { Event } from './event';
import { Node } from './node';
import { ActivatedRoute } from '@angular/router';
import { EventService } from './event.service';
import { NodeService } from './node.service';
import { Router } from '@angular/router';
import {NgIf} from '@angular/common';
declare let d3: any;
import {nvD3} from 'ng2-nvd3';



//Build component sections
@Component({
    selector: 'my-event-detail',
    directives: [nvD3],
    templateUrl: 'app/event-display.component.html',
    styleUrls: ['app/styles/customfonts.css','app/styles/event-details.component.css','app/styles/linechart.css']
     

})
export class EventDisplayComponent implements OnInit, OnDestroy, AfterViewChecked {

    // Initialise variables
    event: Event;
    dateHold: Event;
    minDate: Date;
    maxDate: Date;
    id: string;
    active = true;
    options: any;
    data:any[]  = [];
   
    
    confirmNodeRemove: boolean;

    eventNodeList: Node[] = [];

    filteredList: Node[];

    constructor(
        private eventService: EventService,
        private nodeService: NodeService,
        private route: ActivatedRoute,
        private router: Router) {

    }

    //On component initialisation 
    ngOnInit() {

        //read id information passed in the URL
        this.route.params.subscribe(params => {
            this.id = params['id'];
        });
        //call Event Service function
        //Retrieve and assign the Event object returned by the function and subscribe to the data stream
        this.eventService.getEvent(this.id).subscribe(events => {
            this.event = events;
            this.minDate = new Date(events.startDate.toString());
            this.maxDate = new Date(events.endDate.toString());
           this.dateHold = new Event("", "", "", events.startDate, events.endDate, "");
        });     

        //Give the event service enough time to retrieve the event data to avoid synchronising errors
        setTimeout(() => {

            //Retrieve Nodes/Sensors list from server
            this.nodeService.getNodes()
                .subscribe(
                nodes => {
                    //For each node in the list, add the selected node to the eventNodeList array if its ID matches an id found in the events nodesAttached array
                    for (var i = 0; i < nodes.length; i++) {
                    
                        if (this.event.nodesAttached.includes(nodes[i].id)) {
                        
                            this.eventNodeList.push(nodes[i]);
                        
                        }
                    }
               
                }
            );
            //run the chart initialisation
            this.initChart();

        }, 500);

       
    }

    ngAfterViewChecked() {

        
    }

    

    ngOnDestroy() {
        
    }

   

    //removes a selected node from the event
    //.Subscribe() is used, even though we don't do anything with it when deleting, because Observables are 'lazy' and require the subscription to actually 'execute'
    removeNodeOnEvent(nodeID: string) {
        this.eventService.deleteEventNode(nodeID, this.event.id)
            .subscribe(
        );
        this.hideNodeRemoveConfirm();
        setTimeout(() => {

            location.reload();
        }, 500);
      
    }

    //nav to dynamically created pages
    gotoEdit() {
        let link = ['/edit', this.event.id];
        this.router.navigate(link);
    }

    gotoAttach() {
        let link = ['/attach', this.event.id];
        this.router.navigate(link);
        
    }


    goBack() {
        window.history.back();
    }

   
    showNodeRemoveConfirm() {
        this.confirmNodeRemove = true;
    }

    hideNodeRemoveConfirm() {
        this.confirmNodeRemove = false;
    }
   
    initChart() {
        //Set the dimensions of the canvas of the graph
        var margin = { top: 30, right: 30, bottom: 70, left: 85 },
            width = 960 - margin.left - margin.right,
            height = 500 - margin.top - margin.bottom;

        // Parse the date / time
        var formatTime = d3.time.format("%Y-%m-%d-%H-%M-%S").parse;
        var formatISO = d3.time.format("%Y-%m-%dT%H:%M:%SZ").parse;
        var xAxisTickFormatFunction = function () {
            return function (d:any) {
                return d3.time.format("%Y-%m-%d %H:%M:%S %p")(new Date(d)); 
            }
        }
        //format Tooltip, currently unused due to unidentified object errors
        var toolTipContentFunction = function () {
            return function (d:any) {
                return 'Super New Tooltip' +
                    '<h1>' + d.Node + '</h1>' +
                    '<p>' + d.Detections + ' Detections at ' + d.Time + '</p>'
            }
        }
        //Set the ranges
        var x = d3.time.scale().range([0, width]);
        var y = d3.scale.linear().range([height, 0]);

        //Define the axes
        var xAxis = d3.svg.axis().scale(x)
            .orient("bottom").ticks(5);
        var yAxis = d3.svg.axis().scale(y)
            .orient("left").ticks(5);

        //Define the line
        var valueline = d3.svg.line()
            .x(function (d:any) { return x(d.Time); })
            .y(function (d:any) { return y(d.Detections); });

        //Define the div for the tooltip
        var div = d3.select("body")
            .append("div")
            .attr("class", "tooltip")
            .style("opacity", 0);

        //Adds the svg canvas
        var svg = d3.select("body")
            .append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        this.options = {
            chart: {
                type: 'lineChart',
              //  width: 960 - margin.left - margin.right,
                height: 500 ,
                margin: margin,
                x: function (d:any) { return d.Time; },
                y: function (d:any) { return d.Detections; },
                useInteractiveGuideline: true,
                transitionsDuration: 350,
                
                dispatch: {
                    stateChange: function (e: any) { console.log("stateChange"); },
                    changeState: function (e: any) { console.log("changeState"); },
                    tooltipShow: function (e: any) { console.log("tooltipShow"); },
                    tooltipHide: function (e: any) { console.log("tooltipHide"); }
                },
                xAxis: {
                    axisLabel: 'Time',
                    tickFormat: xAxisTickFormatFunction(),
                    
                },
                yAxis: {
                    axisLabel: 'Detections',
                  //  tickFormat: d3.svg.axis().scale(y)
                  //      .orient("left").ticks(5)
                },
              
                backgroundColor: 'rgb(10,10,10)',
                
                axisLabelDistance: -10
            },
            callback: function (chart: any) {
                console.log("!!! lineChart callback !!!");
            }
        };
           
        
       
        this.data = [];
        var filterArray: any[] = [];

        //Used to dig out of the function scope, its hacky 
        var self = this;

        //Grab data from the tsv on the server
        d3.tsv("http://ltnodeserver.no-ip.org:80/data.tsv", function (error: any, data: any) {
            data.forEach(function (d: any) {

                
                

                //Format the data
                d.Time = formatTime(d.Time);
                d.Detections = +d.Detections;
                
                var dataDate = new Date(d.Time);
                dataDate.setHours(dataDate.getHours());
                
                var minDate = new Date(self.dateHold.startDate.toString());
                var maxDate = new Date(self.dateHold.endDate.toString());

                //Create a filtered array by Serial No. and Date
              

               self.eventNodeList.forEach(function (n: any) {
                   if (n.serial == d.Node && dataDate > minDate && dataDate < maxDate)

                       filterArray.push(d);
                })
               
               
            })
            
            if (error) throw error;
           

             //Nest the entries with Sensor as key            
            self.data = d3.nest()
                .key(function (d: any) {return d.Node;})
                .entries(filterArray);

            
        });
        
     
  
   }



}