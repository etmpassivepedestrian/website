﻿import { Component } from '@angular/core';
import { NgForm }    from '@angular/forms';
import { Event }    from './event';
import { EventService } from './event.service';
//Build component sections 
@Component({
    selector: 'event-form',
    templateUrl: 'app/event-form.component.html',
    styleUrls: ['app/styles/customFonts.css','app/styles/event-form.component.css']

}) 
export class EventFormComponent {


    errorMessage: string;
    events: Event[] = [];
    //Default form values 
    model = new Event('', '', '', new Date(2016, 1, 1,0,0,0,0), new Date(2016, 1, 2) , '');
    submitted = false;
    active = true;
    confirm = false;

    today = new Date().toISOString().split('T')[0];

    //Debug string
    showy: String;

    //Inject Event Service into component
    constructor(private eventService: EventService) { }

    //Code to be executed when the form is submitted 
    //Calls addEvent to send form data to the database
    onSubmit(value: string) {
        
        this.addEvent(value);
        this.submitted = true;
        this.newEvent();
        this.confirm = false;
    }

    
    //Refresh form values after it is submitted
    newEvent() {
        this.model = new Event('0', '', '', new Date(),new Date(),'');
        this.active = false;
        setTimeout(() => this.active = true, 0);
    }

    //Toggles confirm for showing confirmation div tag
    showConfirm() {
        this.confirm = true;
    }

    hideConfirm() {
        this.confirm = false;
    }

    //Called to send form data to the database
    //Value: Form values in string format
    addEvent(value: string) {
        if (!value) { return; }
        //Call the Event Service addEvent function
        //Subscribe to the Observable Event Object and push new event to the end of the array
        //Return an error if problems occur in the service 
        this.eventService.addEvent(value)
            .subscribe(
            event => this.events.push(event),
            error => this.errorMessage = <any>error);
    }


    
}