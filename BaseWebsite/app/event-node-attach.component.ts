﻿import { Component, OnInit} from '@angular/core';
import { Node } from './node';
import { Event } from './event';
import { NodeService } from './node.service';
import { EventService } from './event.service';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/filter';

//Build component sections
@Component({
    selector: 'event-node-attach',
    templateUrl: 'app/event-node-attach.component.html',
    styleUrls: ['app/styles/customfonts.css', 'app/styles/event-node-attach.component.css'],


})

export class EventNodeAttachComponent implements OnInit {

    //Node related variables
    errorMessage: string;
    initNodes: Node[] = [];
    filteredNodes: Node[] = [];
   
    selectedNode: string[];

    //Event being edited variables
    currentEvent: Event;
    sub: any;
    eventId: string;

    constructor(private nodeService: NodeService,
        private eventService: EventService,
        private route: ActivatedRoute) { }

    
    ngOnInit() {
        

        //read id information passed in the URL
        this.route.params.subscribe(params => {
            this.eventId = params['id'];
        });
        //call Event Service function
        //Retrieve and assign the Event object returned by the function and subscribe to the data stream
        this.eventService.getEvent(this.eventId).subscribe(events => this.currentEvent = events);
        
        //run the getNodes function
        setTimeout(() => {
            
            this.getNodes();
        }, 500);
      
    }

    //Retrive nodes from the database using the Node Service
    getNodes() {

        //Call the Node Service 
        //Subscribe to the Observable Node Array returned, filter out any sensors/nodes that the event already has and assign the 'initNodes' array to this array.
        this.nodeService.getNodes()
            .subscribe(
            nodes =>
            {
                for (var i = 0; i < nodes.length; i++)
                {
                    if (!this.currentEvent.nodesAttached.includes(nodes[i].id)) {

                        this.initNodes.push(nodes[i]);
                      
                    }
                }
            }
            );
        
        
      


    }

    //searches throught the filtered array to find any nodes that have been 'checked' for being added to the event
    //Adds these nodes to the event's nodeid array
    //Then updates the event on the server
    attachNodes() {  
        let selected = this.initNodes.filter((x) => x.checked);
        for (var item of selected)
        {
            this.currentEvent.nodesAttached.push(item.id);
        }
        
        this.eventService.updateEventNodeList(this.currentEvent)
            .subscribe(
            error => this.errorMessage = <any>error);

        //re-route back to detail page
        setTimeout(() => {
            window.history.back();
        }, 400);
        
    }

    //Toggles checked value of sensor, may not be needed if [(ngmodel)] does what its supposed to do
    //BTW It didnt do what it was supposed to...
    checkbox(onNode:Node) {
        onNode.checked = (onNode.checked) ? false : true;
    }


    
   
    



}
