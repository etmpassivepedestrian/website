﻿import { Injectable }     from '@angular/core';
import { Http, Response } from '@angular/http';
import { Event }           from './event';
import { Observable }     from 'rxjs/Observable';
import './rxjs-operators';
import { Headers, RequestOptions } from '@angular/http';

//Retrieves and sends data to and from the server
@Injectable()
export class EventService {
    constructor(private http: Http) { }
    private eventsUrl = 'http://ltnodeserver.no-ip.org/api/events';  // URL to web API

    //Retrieves all events on server 
    getEvents(): Observable<Event[]> {
        return this.http.get(this.eventsUrl)
            .map(this.extractData)
            .catch(this.handleError);
    }

    //Converts response object to json data 
    private extractData(res: Response) {
        let body = res.json();
        return body.data || {};
    }
    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

    //Adds event to database
    addEvent(value: string): Observable<Event> {

        //In order to fix an issue regarding Date conversion between server and website 
        //we need to pass a custom header that allows the server to dermine how many hours to 'edit' the dates by
        let temp = new Date().getTimezoneOffset();

         //Turn string format into javascript json format 
        //Generate http Headers
        //Generate request options
        let body = JSON.stringify(value);
        
        let headers = new Headers({ 'Content-Type': 'application/json', 'offset': temp });
        let options = new RequestOptions({ headers: headers });

        //post request to server
        return this.http.post(this.eventsUrl, body, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    //updates event to database
    updateEvent(id: string, value: string): Observable<Event> {

        //In order to fix an issue regarding Date conversion between server and website 
        //we need to pass a custom header that allows the server to dermine how many hours to 'edit' the dates by
        let temp = new Date().getTimezoneOffset();
        
        //Turn string format into javascript json format 
        //Generate http Headers
        //Generate request options
        let body = JSON.stringify(value);
        
        let headers = new Headers({ 'Content-Type': 'application/json', 'offset': temp});
        let options = new RequestOptions({ headers: headers });

        //post request to server
        return this.http.put(this.eventsUrl + '/'+ id, body, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    //Different method required to update an events list of sensors
    //Takes an Event object as in this case we won't be taking a form's submit value, but rather the event which contains newly assigned sensor nodes
    //Process of sending the data however is syntactically identical because JSON.stringify is awesome.
    updateEventNodeList(eventTemp: Event)
    {


        //In order to fix an issue regarding Date conversion between server and website 
        //we need to pass a custom header that allows the server to dermine how many hours to 'edit' the dates by
        let temp = 0;

        let body = JSON.stringify(eventTemp);

        let headers = new Headers({ 'Content-Type': 'application/json', 'offset': temp });
        let options = new RequestOptions({ headers: headers });

        //post request to server
        return this.http.put(this.eventsUrl + '/' + eventTemp.id, body, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    deleteEvent(eventID: string) {
        return this.http.delete(this.eventsUrl + '/' + eventID)
            .map(this.extractData)
            .catch(this.handleError);
    }

    deleteEventNode(nodeID: string, eventID: string) {
       
        return this.http.delete(this.eventsUrl + '/' + eventID + '/' + 'nodesAttached' + '/' + nodeID)
            .map(this.extractData)
            .catch(this.handleError);
    }

    //retrieve specific event based on id
    getEvent(id: string) {
        return this.getEvents()
            .map(events => events.find(event => event.id == id));
    }

}
